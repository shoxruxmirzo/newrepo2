const telegraf = require('telegraf')
const data = require('./data.json')
const Stage = require('telegraf/stage')
const session = require('telegraf/session')
const Scene = require('telegraf/scenes/base')
const {leave} = Stage
const stage = new Stage()

// id va tokenlars
const admin = data.channel_id
const token = data.token


// assigning variables
const bot = new telegraf(token) // token data.json da 
const parse = {parse_mode: "Markdown"}



// kiritish uchun 
const getName = new Scene('getName')
stage.register(getName)
const getNum = new Scene('getNum')
stage.register(getNum)
const getAddress = new Scene('getAddress')
stage.register(getAddress)
const getAboutFamily = new Scene('getAboutFamily')
stage.register(getAboutFamily)
const check = new Scene('check')
stage.register(check)

// const getDay = new Scene('getDay')
// stage.register(getDay)
// const getTime = new Scene('getTime')

// stage.register(getTime)

bot.use(session())
bot.use(stage.middleware())

bot.hears('◀ Bosh menyu', (ctx) => {
    ctx.reply(
        '*Ism-Familiyangizni  kiriting* ⬇',
        {parse_mode: "Markdown", reply_markup: { remove_keyboard: true} }
    )
    ctx.scene.enter('getName')
})

bot.start((ctx) => {
    ctx.reply(
        `Salom, *${ctx.from.first_name}*\n\n💰 Yelkama-yelka Qo'qon Koronavirusga qarshi "Mehr muruvvat" jamoat xayriya fondiga xush kelibsiz.`,
    )

    ctx.reply(
        `*Ism-familiyangizni kiriting* ⬇`,
    )
        {{ parse_mode: "Markdown"} reply_markup: { remove_keyboard: true} }

    ctx.scene.enter('getName')
})

// ismni kiritish
getName.command('start', async (ctx) => {
    ctx.reply(
        `Boshidan boshlaymiz.\n*Ism-familiyangizni kiriting* ⬇`,
        {parse_mode: "Markdown", reply_markup: { remove_keyboard: true }}
    )
    await ctx.scene.leave('getAboutFamily')
    ctx.scene.enter('getName')
})

getName.on('text', async (ctx) => {
    if (ctx.message.text === '◀ Orqaga') {
        return ctx.reply( 'Siz registratsiyaning boshidasiz. *Iltimos ism-familiyangizni kiriting* ⬇')
    }
  ctx.session.name = ctx.message.text

  // manzilni kiritib olish
  ctx.reply(
      `🏠 *Manzilni to'liq kiriting*\n\n❗ *Eslatma:* _Uy raqami, ko'cha, tuman/shahar to'liq yozilsin_`,
      {parse_mode: "Markdown", reply_markup: { keyboard: [`◀ Orqaga`],
       resize_keyboard: true, on_time_keyboard: true }}
  )  
  await ctx.scene.leave('getName')
  ctx.scene.enter('getAddress')
})

//addressmi kiritish
getAddress.on('text', async (ctx) => {
    ctx.session.address = ctx.message.text
    ctx.reply(
        `👪 *Oila haqida qisqacha yozing?*`,
        {parse_mode: "Markdown", reply_markup: { keyboard: [[`◀ Orqaga`, `❌ O'chirish`]], resize_keyboard: true, on_time_keyboard: true }}
    )
    await ctx.scene.leave('getAddress')
    ctx.scene.enter('getAboutFamily')
} )

getAddress.hears( '◀ Orqaga', async (ctx) => {
    ctx.reply(
        '*Ism-familiyangizni kiriting* ⬇',
        {parse_mode: "Markdown", reply_markup: { remove_keyboard: true} }
    )
    await ctx.scene.leave('getAddress')
    ctx.scene.enter('getName')
})



// Oila haqida ma'lumot olish
getAboutFamily.hears('◀ Orqaga', async (ctx) => {
    ctx.reply(
        `🏠 *Manzilni to'liq kiriting*\n\n❗ *Eslatma:* _Uy raqami, ko'cha, tuman/shahar to'liq yozilsin_`,
        {parse_mode: "Markdown", reply_markup: { keyboard: [[ `◀ Orqaga`, `❌ O'chirish`]], resize_keyboard: true, on_time_keyboard: true }}
    )
    await ctx.scene.leave('getAboutFamily')
    ctx.scene.enter('getAddress')
})

getAboutFamily.hears([ `❌ O'chirish`, `/start`], async (ctx) => {
    ctx.reply(
        `Boshidan boshlaymiz.\n*Ism-familiyangizni kiriting* ⬇`,
        {parse_mode: "Markdown", reply_markup: { remove_keyboard: true }}
    )
    await ctx.scene.leave('getAboutFamily')
    ctx.scene.enter('getName')
})


// tel nomerni optional ham qilish kerak
getAboutFamily.on('text', async (ctx) => {
    ctx.session.familyInfo = ctx.message.text
    ctx.reply(
        `📞 *Telefon raqamingizni yuboring*`,
        { parse_mode: "Markdown", reply_markup: { keyboard: [[{text: '📞 Telefon raqamni yuborish', request_contact: true}],
        [ `◀ Orqaga`, `❌ O'chirish`]], resize_keyboard: true, on_time_keyboard: true }}
    )
    await ctx.scene.leave('getAboutFamily')
    ctx.scene.enter('getNum')
})

//raqamni kiritish
getNum.hears('◀ Orqaga', async (ctx) => {
    ctx.reply(
        `👪 *Oila haqida qisqacha yozing?*`,
        {parse_mode: "Markdown", reply_markup: { keyboard: [[ `◀ Orqaga`, `❌ O'chirish`]], resize_keyboard: true, on_time_keyboard: true }}
    )
    await ctx.scene.leave('getNum')
    ctx.scene.enter('getAboutFamily')
})

getNum.hears([`❌ O'chirish`, `/start`], async (ctx) => {
    ctx.reply(
        `Boshidan boshlaymiz.\n*Ism-familiyangizni kiriting* ⬇`,
        {parse_mode: "Markdown", reply_markup: { remove_keyboard: true }}
    )
    await ctx.scene.leave('getNum')
    ctx.scene.enter('getName')
    ctx.session = null
})


// shu yerda phone num orniga oddiy number bolishi ham mumkin deb qoyish kere
getNum.on('contact', async (ctx) => {
    ctx.session.num = ctx.message.contact.phone_number
    ctx.reply (
        `❗_So'rovnomani yuborishdan avval ma'lumotlarni qayta tekshirib ko'ring._\n\n*✅ Jo'natish" tugmasini bosing*` +
        `\n\nMa'lumotlar:\nIsm-Familiya: ${ctx.session.name}\nManzil ${ctx.session.address}\nOila haqida: ${ctx.session.familyInfo}`+
        `\nTelefon raqam: ${ctx.session.num}`,

        {parse_mode: "Markdown", reply_markup: { keyboard: [[ `✅ Jo'natish`],
            [ `◀ Orqaga`, `❌ O'chirish`]], resize_keyboard: true, on_time_keyboard: true }}
    )
    await ctx.scene.leave('getNum')
    ctx.scene.enter('check')
})

check.hears(`✅ Jo'natish`, (ctx) => {
    ctx.reply(
        `✅ Raxmat. Siz ro'yxatga olindingiz. Tez orada siz bilan bog'lanamiz 😊` +
        `\n\nYelkama-yelka - Qo'qon Koronavirusga qarshi "Mehr muruvvat" jamoat xayriya fondi.\n*Telefon:* +998999232523\n📱 *Telegram kanal:* @yelkamayelka`,
        {parse_mode: "Markdown", reply_markup: { keyboard: [['◀ Bosh menyu']], resize_keyboard: true, on_time_keyboard: true } }
    )
    ctx.scene.leave('main')

    //adminga xabar yuborish
    ctx.telegram.sendMessage(admin,
        `Yangi a'zo!\n\nIsm-Familiya: [${ctx.session.name}]\nUsername: @${ctx.from.username}\nManzil: ${ctx.session.address}\nOila haqida: ${ctx.session.familyInfo}\nKunlar: ${ctx.session.day}` +
        `\nTelefon raqami: ${ctx.session.num}` )
    

    console.log(`Yangi a'zo!\n\nIsm-Familiya: [${ctx.session.name}]\nUsername: @${ctx.from.username}\nManzil: ${ctx.session.address}\nOila haqida: ${ctx.session.familyInfo}\nKunlar: ${ctx.session.day}` +
    `\nTelefon raqami: ${ctx.session.num}\n`);


    ctx.session = null
})




check.hears('◀ Orqaga', async (ctx) => {
    ctx.reply(
        `Telefon raqamingizni yuboring`+
        `\n\nMa'lumotlar:\nIsm-Familiya: ${ctx.session.name}\nManzil ${ctx.session.address}\nOila haqida: ${ctx.session.familyInfo}`,
        { reply_markup: { keyboard: [[{text: '📞 Telefon raqamni yuborish', request_contact: true}],
        [ `◀ Orqaga`, `❌ O'chirish`]], resize_keyboard: true, on_time_keyboard: true }}
    )
    await ctx.scene.leave('check')
    ctx.scene.enter('getNum')
})

check.hears([`❌ O'chirish`, `/start`], async (ctx) => {
    ctx.reply(
        `Boshidan boshlaymiz.\n*Ism-familiyangizni kiriting* ⬇`,parse,
        { reply_markup: { remove_keyboard: true }}
    )
    await ctx.scene.leave('check')
    ctx.scene.enter('getName')

    ctx.session = null
})



console.log('INDEXXX is LIVE');


bot.catch((err, ctx) => {
    console.log(`Ooops, ecountered an error for ${ctx.updateType}`, err)
  })

bot.startPolling()